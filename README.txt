Introduction
-----------
This module is used to delete the 
Comments for the given date range, Content type and 
Comment Type which is given by you.

This module will add a Menu tab(Comment Bulk Delete) 
under the Comment tab in the Content Section.

Here you can give your required date range and 
content type names(Multiselect Possible) and 
Comment Type(Multiselect Possible) for delete the comments.

Requirements
------------
Drupal 7.x
Comment 7.x
Date pop up
Date api


Installation
------------
1. Copy the entire comment_bulk_delete module directory and place it inside the
   Drupal sites/all/modules directory.

2. Login as an administrator. 
   Enable the module in the "Administrator" -> "Modules" -> "Others"

How to use:
-----------
1.After installation login as admininstrator.
2.Go to admin/content/comment/comment_bulk_delete .
3.Give your require date range, content type name 
  and comment type for performing Comment Delete operation.
